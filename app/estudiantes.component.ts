 import { Component } from '@angular/core'
 
 @Component({
     selector : 'estudiantes',
                         {{estudiante}}
                  
   
    template : `<div>
                    <h3>{{titulo}}</h3> 
                    <ul>
                        <li *ngFor="let estudiante of estudiantes"> 
                            {{estudiante}}
                        </li>
                    </ul>
                    <img [src]="imgUrl"/>                    
                    <input type="text" [(ngModule)] = "titulo"/>
                    <button (click)="alerta()">BorrarTitulo</button>
                </div>
                `          
})
 export class EstudiantesComponent{
     titulo = "Lista de Estudiantes";
     estudiantes = ['Graciela Celis', 'Alejandra Muñoz', 'Natalia Forero','Fredy Vargas','Ronal David Hoyos'];
     imgUrl = "http://lorempicsum.com/futurama/255/200/2";
     
     alerta(){
         this.titulo="";       
     }
 }